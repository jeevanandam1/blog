/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		"./pages/**/*.{js,ts,jsx,tsx}",
		"./components/**/*.{js,ts,jsx,tsx}",
	],
	theme: {
		extend: {
			colors: {
				"theme-gray-100": "#696871",
				"theme-gray-200": "#eae9f2",
				"theme-gray-300": "#767581",
				"theme-gray-400": "#19191b",
				"theme-voilet-400": "#5454d4",
				"theme-red-400": "#f04037",
				"theme-red-500": "#c31a12",
			},
		},
	},
	plugins: [],
};
