const Title = () => {
	// Title
	return (
		<section className="px-4 text-center">
			<p className="pt-12 text-4xl font-bold md:pt-44 md:text-7xl text-theme-gray-400">
				Blog Regular
			</p>
			<p className="w-11/12 mx-auto text-base font-normal tracking-wider lg:w-6/12 pb-14 md:text-xl md:pb-32 pt-7 text-theme-gray-100">
				Create custom landing pages with Omega that converts more visitors than
				any website.
			</p>
		</section>
	);
};

export default Title;
