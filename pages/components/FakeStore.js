import Image from "next/image";
import Link from "next/link";

const FakeStore = ({ value, btnText }) => {
	// Fakestore
	return (
		<div className="flex flex-wrap justify-center w-11/12 pb-4 mx-auto 2xl:w-6/12 gap-y-10 gap-x-7">
			{value &&
				value.map((detail) => (
					<div
						className="transition duration-300 ease-in-out delay-150 border rounded-lg border-theme-gray-200 w-80 hover:shadow-xl"
						key={detail.id}
					>
						<div className="my-4 text-center">
							<Image src={detail.image} alt="Banner" width={100} height={100} />
						</div>
						<div className="px-5">
							<p className="py-4 text-base capitalize text-theme-gray-300">
								{detail.category}
							</p>
							<p className="pb-3 text-xl font-bold leading-6 truncate -tracking-wider text-theme-gray-400">
								{detail.title}
							</p>
							<p className="h-20 py-2 overflow-x-hidden overflow-y-hidden text-theme-gray-300">
								{detail.description}
							</p>
							<Link href="https://nextjs.org/">
								<a>
									<p className="pt-4 pb-6 text-base font-bold text-theme-red-400 -tracking-wider">
										{btnText}
									</p>
								</a>
							</Link>
						</div>
					</div>
				))}
		</div>
	);
};

export default FakeStore;
