import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";

import styles from "./Paginate.module.css";
import FakeStore from "./FakeStore";

// import Items from "./Items";

const PaginatedItems = ({ itemsPerPage, initial, btnText }) => {
	const [currentItems, setCurrentItems] = useState(null);
	const [pageCount, setPageCount] = useState(0);
	const [itemOffset, setItemOffset] = useState(0);

	useEffect(() => {
		const endOffset = itemOffset + itemsPerPage;
		setCurrentItems(initial.slice(itemOffset, endOffset));
		setPageCount(Math.ceil(initial.length / itemsPerPage));
	}, [itemOffset, itemsPerPage, initial]);

	const handlePageClick = (event) => {
		const newOffset = (event.selected * itemsPerPage) % initial.length;
		setItemOffset(newOffset);
	};

	return (
		<>
			{/* <Items currentItems={currentItems} /> */}
			<FakeStore value={currentItems} btnText={btnText} />
			<div
				className={`mx-auto my-4 lg:mb-24 w-11/12 lg:w-8/12 ${
					itemsPerPage === 6 ? styles.paginated : styles.paginate
				}`}
			>
				<ReactPaginate
					breakLabel="..."
					nextLabel=">"
					onPageChange={handlePageClick}
					pageRangeDisplayed={5}
					pageCount={pageCount}
					previousLabel="<"
					renderOnZeroPageCount={null}
				/>
			</div>
		</>
	);
};

export default PaginatedItems;
