import Link from "next/link";

import { FaFacebookF } from "react-icons/fa";
import { FaTwitter } from "react-icons/fa";
import { BsGoogle } from "react-icons/bs";

const Footer = () => {
	const About = ["Our mission", "Our stroy", "Team members"];
	const Learn = ["Tutorials", "How it works", "FAQ"];
	const Stories = ["Blog", "Tech stories"];
	const Hire = ["Career", "Freelancers", "Trainee"];

	return (
		<section className="bg-[url('/background.svg')] bg-no-repeat bg-cover bg-bottom lg:bg-top min-h-[80vh]  border-t">
			<div className="flex flex-col items-center justify-center min-h-[50vh] md:min-h-[80vh] text-center">
				<p className="text-2xl font-bold text-white lg:text-7xl -tracking-wide">
					Ready to launch?
				</p>
				<p className="w-11/12 px-4 mx-auto my-8 text-base text-center text-white lg:w-5/12 opacity-70">
					Are you interested to join our team? Check out our job openings and
					apply on your suitable role.
				</p>
				<Link href="https://nextjs.org/">
					<a className="px-4 py-2 text-sm duration-75 transition-all font-medium text-white rounded hover:-translate-y-0.5 lg:text-base bg-theme-red-500">
						Get Omega Now
					</a>
				</Link>
			</div>
			<div className="flex flex-wrap items-start justify-between w-11/12 pb-24 mx-auto text-white md:flex-nowrap lg:w-9/12">
				<div className="w-full">
					<p className="py-4 text-2xl font-black md:p-0">Omega</p>
				</div>
				<div className="w-full">
					<p className="py-2 text-base font-bold md:pb-5">About</p>
					{About.map((x) => (
						<div className="py-2" key={x}>
							<Link href="https://nextjs.org/">
								<a className="transition-all duration-75 hover:opacity-100 opacity-70">
									{x}
								</a>
							</Link>
						</div>
					))}
				</div>
				<div className="w-full">
					<p className="py-2 text-base font-bold md:pb-5">Learn</p>
					{Learn.map((x) => (
						<div className="py-2" key={x}>
							<Link href="https://nextjs.org/">
								<a className="transition-all duration-75 hover:opacity-100 opacity-70">
									{x}
								</a>
							</Link>
						</div>
					))}
				</div>
				<div className="w-full">
					<p className="py-2 text-base font-bold md:pb-5">Stories</p>
					{Stories.map((x) => (
						<div className="py-2" key={x}>
							<Link href="https://nextjs.org/">
								<a className="transition-all duration-75 hover:opacity-100 opacity-70">
									{x}
								</a>
							</Link>
						</div>
					))}
				</div>
				<div className="w-full">
					<p className="py-2 text-base font-bold md:pb-5">Hire</p>
					{Hire.map((x) => (
						<div className="py-2" key={x}>
							<Link href="https://nextjs.org/">
								<a className="transition-all duration-75 hover:opacity-100 opacity-70">
									{x}
								</a>
							</Link>
						</div>
					))}
				</div>
			</div>
			<hr className="mx-auto md:w-9/12" />
			<div className="items-center justify-between block px-2 py-6 mx-auto md:flex md:w-9/12 ">
				<div>
					<p className="text-sm font-normal text-center text-white opacity-70 md:text-base">
						&#64; 2022 UXTheme, All Rights Reserved
					</p>
				</div>
				<div className="flex justify-center gap-4 py-4 text-white md:justify-end">
					<Link href="https://nextjs.org/">
						<a className="hover:text-theme-voilet-400">
							<FaTwitter />
						</a>
					</Link>
					<Link href="https://nextjs.org/">
						<a className="hover:text-theme-voilet-400">
							<FaFacebookF />
						</a>
					</Link>
					<Link href="https://nextjs.org/">
						<a className="hover:text-theme-voilet-400">
							<BsGoogle />
						</a>
					</Link>
				</div>
			</div>
		</section>
	);
};

export default Footer;
