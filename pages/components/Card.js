// import FakeStore from "./FakeStore";
// import Sample from "./Sample";

import PaginatedItems from "./PaginatedItems";

const Card = ({ value, details, btnText }) => {
	// Card
	return (
		<section>
			{/* <Sample details={details} /> */}
			{/* <FakeStore value={value} btnText={btnText} /> */}
			<PaginatedItems itemsPerPage={6} initial={value} btnText={btnText} />
		</section>
	);
};

export default Card;
