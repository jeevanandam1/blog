import { FcMenu } from "react-icons/fc";
import Link from "next/link";

const Navbar = () => {
	return (
		<div className="flex items-center justify-between p-4">
			<div>
				<Link href="/">
					<a className="text-2xl font-black text-theme-gray-400">Omega</a>
				</Link>
			</div>
			<div className="items-center justify-between block gap-8 md:hidden">
				<FcMenu />
			</div>
			<div className="items-center justify-between hidden gap-8 md:flex">
				<Link href="/">
					<a className="text-base text-theme-gray-400 hover:text-theme-voilet-400">
						Demo
					</a>
				</Link>
				<Link href="/">
					<a className="text-base text-theme-gray-400 hover:text-theme-voilet-400">
						Pages
					</a>
				</Link>
				<Link href="/">
					<a className="text-base text-theme-gray-400 hover:text-theme-voilet-400">
						Support
					</a>
				</Link>
				<Link href="/">
					<a className="p-4 text-base font-medium text-white rounded bg-theme-red-500">
						Get Started
					</a>
				</Link>
			</div>
		</div>
	);
};

export default Navbar;
