import Image from "next/image";
import Link from "next/link";

const Sample = ({ details }) => {
	// Sample
	return (
		<div className="flex flex-wrap justify-center w-11/12 pb-4 mx-auto gap-y-10 gap-x-7">
			{details &&
				details.map((detail) => (
					<div
						className="transition duration-300 ease-in-out delay-150 border rounded-lg border-theme-gray-200 w-80 hover:shadow-xl"
						key={detail.id}
					>
						<Image src={detail.img} alt="Banner" width={336} height={336} />
						<div className="px-5">
							<p className="py-4 text-base text-theme-gray-300">
								{detail.date}
							</p>
							<p className="pb-3 text-xl font-bold leading-6 -tracking-wider text-theme-gray-400">
								{detail.heading}
							</p>
							<p className="py-2 text-base text-theme-gray-300">
								{detail.para}
							</p>
							<Link href="https://nextjs.org/">
								<a>
									<p className="pt-4 pb-6 text-base font-bold cursor-pointer text-theme-red-400 -tracking-wider">
										{detail.btnText}
									</p>
								</a>
							</Link>
						</div>
					</div>
				))}
		</div>
	);
};

export default Sample;
